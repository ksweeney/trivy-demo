# trivy-demo

demo of utilizing [trivy](https://github.com/aquasecurity/trivy) to scan containers and images

* Scans a project's build of a Dockerfile
* Scans a registry image
* Scans a running container's filesystem
* Scans a remote repository

---
trivy-demo is licensed under unlicense

trivy-demo project is not affiliated with or endorsed by aqua security

[trivy is licensed under Apache 2.0](https://github.com/aquasecurity/trivy/blob/master/LICENSE)

<div>Icon made by <a href="http://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>